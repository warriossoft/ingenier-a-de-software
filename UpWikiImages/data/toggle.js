var ini = self.options.init;

function options(option){
	//self.port.emit('Toggle_init', self.options.init);
	if(option=="capture"){
		self.port.emit('capture');
		self.postMessage({
			name: "capture"
		})
	}
	if(option=="login"){
		self.port.emit('login');
		self.postMessage({
			name: "login"
		})
	}
}
exportFunction(options, unsafeWindow, {defineAs: "options"});