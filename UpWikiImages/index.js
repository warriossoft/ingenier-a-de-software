var self = require('sdk/self');
var menuitem = require("menuitem");
var pageMod = require("sdk/page-mod");
var windows = require("sdk/windows").browserWindows;
var notifications = require("sdk/notifications");
var store_credentials = require("sdk/passwords");
//**********************************************************

var data = require('sdk/self').data;
var Request = require('sdk/request').Request;

//***********************************************************
var {Cc, Ci} = require("chrome");
var us="";
var pwd="";

function dummy(text, callback) {
  callback(text);
}

exports.dummy = dummy;
var buttons = require('sdk/ui/button/action');
var tabs = require("sdk/tabs");
var { modelFor } = require("sdk/model/core");
var { viewFor } = require("sdk/view/core");
var browserWindows = require("sdk/windows").browserWindows;
/*var button = buttons.ActionButton({
  id: "UpWikiImages",
  label: "UpWikiImages",
  icon: {
    "32": "./icon32.png",
    "64": "./icon64.png"
  },
  onClick: handleClick
});
*/

//****************************************************************************************

var buttons = require('sdk/ui/button/toggle');
var panels = require("sdk/panel");

var button = buttons.ToggleButton({
  id: "UpWikiImages",
  label: "UpWikiImages",
  icon: data.url("./icon32.png"),
  onChange: handleChange
});

var panel = panels.Panel({
  width: 400,
  height: 300,
  contentURL: data.url("toggle.html"),
  contentScriptFile: data.url("./toggle.js"),
      contentScriptOptions: {
            init: "Select your option!"
          },
  onMessage: function(event){
    if(event.name){
      switch (event.name){
        case "capture":
          verifycookie();
          break;
        case "login":
          login();
          break;
      }
    }
  },
  onHide: handleHide
});

function handleChange(state) {
  if (state.checked) {
    panel.show({
      position: button
    });
  }
  //actions();
}

function handleHide() {
  button.state('window', {checked: false});
  panel.port.emit("onHide");
}

function actions(){
  handleHide();
  panel.port.on('capture', function() {
      verifycookie();
  });

  panel.port.on('login', function() {
      login();
  });

  panel.port.on('Toggle_init', function(init) {
      notify(init);
  });
}


//****************************************************************************************

var menuitem = menuitem.Menuitem({
  id: "UpWikiImages",
  menuid: "menu_ToolsPopup",
  label: "UpWikiImages",
  image: self.data.url("./icon.png"),
  onCommand: function() {
    login();
  },
  insertbefore: "menu_pageInfo"
});

function login() {
    var myPanel = require("sdk/panel").Panel({
      position: {
    top: 100,
    bottom: 190,
  },
  height: 370,
  width: 450,
      contentURL: "https://en.wikipedia.org/w/index.php?title=Special:UserLogin&returnto=Main+Page"/*,
      contentScriptFile: "./login.js",
      contentScriptOptions: {
            init: "Login UpWikiImages"
          }*/
    });
    /*myPanel.port.on('credentials', function(user, pass) {
                    myPanel.hide();
                  });
    myPanel.port.on('login_init', function(init) {
                    notify(init);
                  });*/
  myPanel.show();
}
function save_credentials(user, pass) {
  
     store_credentials.store({
                      realm: "UpWikimages",
                      username: user,
                      usernameField: "username",
                      password: pass,
                      passwordField: "password",
                      onError: function(){

                      },
                      onComplete: function(){
                        notify("user and password store successfully");
                        us=us+user;
                        pwd=pwd+pass;
                      }
                    }); 
}

function verify_pass(usr) {
  var cred="";
  store_credentials.search({
    realm: "UpWikimages",
    onComplete: function onComplete(credentials) {
          credentials.forEach(function(credential) {
            cred="Credentials***"+credential.username+"***"+credential.password;
            
            if(credential.username!=""){
            notify("Checking Credentials from Wimedia");
          }
          else{
            login();
          }
            });
      }
    });
  }

function notify(msg) {
    notifications.notify({
      title: "Status",
      text: msg,
      data: "UpWikiImages Notification",
      onClick: function (data) {
        console.log(data);
      }
    });
}

function handleClick(state) {
  console.log(tabs.activeTab.url + " is loaded");
  verifycookie();
}

function tab(user,password){
  var ur = tabs.activeTab.url;
  var cred = "Credentials***"+user+"***"+password;
  console.log("In index: "+cred);
  if(ur.indexOf("eso.org/")==-1 && ur.indexOf("spacetelescope.org/")==-1){
        tabs.activeTab.attach({
          contentScriptFile: "./content-script.js",
          contentScriptOptions: {
            a: "Url no valida, por favor ingrese una url de eso.org"
          }
      });
  }
  
  else{
   tabs.open({
          url: "test.html",
          onReady: function(tab) {
            worker = tab.attach({
              contentScriptFile: "connection.js",
              contentScriptOptions: {
                url: ur
              }        
            });

            console.log("antes de getcookie");
            worker.port.emit('authentication', getcookie());

            worker.port.on('loaded', function(pageInfo) {
              notify(pageInfo);
            });
          }
    });
  }
}

function getcookie(){

var user;
var token;
var cookieManager = Cc["@mozilla.org/cookiemanager;1"].getService(Ci.nsICookieManager2);
var count = cookieManager.getCookiesFromHost(".wikipedia.org");

var creden = "Credentials***";

while (count.hasMoreElements()){
    var cookie = count.getNext();
    if (cookie instanceof Ci.nsICookie){
        if(cookie.name=="centralauth_User"){
          creden=creden+cookie.value;
          user=cookie.value;
        }
        if(cookie.name=="centralauth_Session"){
          creden=creden+"***"+cookie.value;
          token=cookie.value;
        }
    }
}

console.log("cookie"+creden);
return creden;
}

function verifycookie(){
  var user;
  var token;
  var cookieManager = Cc["@mozilla.org/cookiemanager;1"].getService(Ci.nsICookieManager2);
  var count = cookieManager.getCookiesFromHost(".wikipedia.org");

  while (count.hasMoreElements()){
      var cookie = count.getNext();
      if (cookie instanceof Ci.nsICookie){
          if(cookie.name=="centralauth_User"){
            user=cookie.value;
          }
          if(cookie.name=="centralauth_Session"){
            token=cookie.value;
          }
      }
  }

  if(token==null){
    login();
  }
  else{
    tab(user,token);
  }
}