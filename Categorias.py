# Keywords
def getList(string):
    test1 = string.split(",")
    test2 = []
    x=0
    for i in test1:
        if(x==0):
            test2.append(i[2:-1])
        elif(x==(len(test1))-1):
            test2.append(i[2:-2])
        else:
            test2.append(i[1:-1])
        x=x+1
    return test2

def keywords(test_url):
    response = alchemyapi.keywords('url', test_url)
    key = response['keywords']
    keyword = []
    for i in range(len(key)):
        if float(key[i]['relevance']) >= 0.70:
            keyword.append(key[i]['text'])
    return keyword

# Category
def category(test_url):
    response = alchemyapi.category('url', test_url)
    category = []
    category = response['category']
    return category

def getSubject(file):
    image = file
    metadata = ImageMetadata(image)
    metadata.read()
    pal_claves = format(metadata['Xmp.dc.subject'].raw_value)

    return getList(pal_claves)

def Categorias(file, page_url):
    categorias = []
    categorias.append("Picture Upload by WarriorSoft-Team")
    palabras_claves = getSubject(file)
    for cat in palabras_claves:
        categorias.append(cat)
    for cat in keywords(page_url):
        categorias.append(cat)
    cat = category(page_url)
    categorias.append(cat)
    categorias_1 = ""
    for aux in categorias:
        categorias_1 += "[[Category:" + aux + "]]\n"

    #print(categorias_1)
    return categorias_1

Categorias("Images.jpg", "Ruta de la Imagen")
