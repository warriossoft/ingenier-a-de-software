#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests


texto = "To celebrate the fast pace of the construction work of the ESO Supernova Planetarium & Visitor Centre, a festivity was held on 28 October 2015, at ESO Headquarters in Garching bei München, Germany. The construction workers were invited by ESO, the architects Bernhardt + Partner, and other companies involved in the construction including Schumann Projektsteuerung,Bollinger + Grohmann, IB Hausladen and Burnickl Ingenieure, to a party with food and beer. The whole occasion provided both the construction team and the future occupants with an opportunity to celebrate the building’s progress and to wish it and its occupants luck in the future."

texto = texto.lower()
texto = texto.replace(" to ", " ")
texto = texto.replace(" the ", " ")
texto = texto.replace(" of ", " ")
texto = texto.replace(" a ", " ")
texto = texto.replace(" was ", " ")
texto = texto.replace(" & ", " ")
texto = texto.replace(" + ", " ")
texto = texto.replace(" on ", " ")
texto = texto.replace(" at ", " ")
texto = texto.replace(" in ", " ")
texto = texto.replace(" an ", " ")
texto = texto.replace(" and ", " ")
texto = texto.replace(" it ", " ")
texto = texto.replace(" its ", " ")
texto = texto.replace(".", "")
texto = texto.replace(",", " ")



texto_separado = texto.split(" ")

for a in texto_separado:
    print (a)
URL = "https://en.wikipedia.org/wiki/"
for a in texto_separado:
    url_final = URL + a
    response = requests.get(url_final)
    rta = response.status_code
    if(rta == 200):
        print(("[+] OK ->    " + url_final))
