# -*- coding: utf-8 -*-
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                   MINI-SDK (OFICIAL) DE WARRIOR_SOFT                  #
#        Name: esoWiki_WS.py                                            #
#        Authors: diego1996 - maxiners - erekishgal - jhonmario         #
#        Team: WarriosSoft                                              #
#        Version: 1.0                                                   #
#        Description: MiniSDK for Working Image Scraping, Metadatas of  #
#                     Pictures, IBM Watson, and Wikimedia               #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#   _       _ _____ _____  _____  _ _____ _____  _____                  #
#  | |     | |  _  |  _  \|  _  \| |  _  |  _  \|  ___|                 #
#  | |  _  | | |_| | |_| || |_| || | | | | |_| || |___                  #
#  | | | | | |  _  |  _  \|  _  \| | | | |  _  \|___  |                 #
#  | |_| |_| | | | | | \ \| | \ \| | |_| | | \ \ ___| |                 #
#  |_________|_| |_|_|  \_|_|  \_|_|_____|_|  \_|_____|                 #
#                                                                       #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#from __future__ import print_function

import urllib
import requests
from pyquery import PyQuery as pq
from alchemyapi import AlchemyAPI
from pyexiv2 import ImageMetadata
from cookielib import Cookie
import wikitools
from wikitools import api
import sys
import time
import os
try:
    import cPickle as pickle
except:
    import pickle

alchemyapi = AlchemyAPI()
wiki_url = 'https://commons.wikimedia.org/w/api.php'
wiki_username = "acaronte"
wiki_password = "warrios"

def Login(username,password):
    wiki = wikitools.wiki.Wiki(wiki_url)
    try:
        print "login"

        '''
        c = Cookie(version=0, name='centralauth_Session', value='b8acd1bdaf2302f2bce490fa16d81d0', port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=True, expires=None, discard=True, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)>
        c = Cookie(version=0, name='centralauth_Token', value='6b3100dac3d7b04e0dd79eb4b5670f61', port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=True, expires=1451355118, discard=False, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        c = Cookie(version=0, name='centralauth_User', value='Acaronte', port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=True, expires=1451355118, discard=False, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        c = Cookie(version=0, name='forceHTTPS', value='1', port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=1451355118, discard=False, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        c = Cookie(version=0, name='GeoIP', value='US:WA:Redmond:47.68:-122.12:v4', port=None, port_specified=False, domain='.wikimedia.org', domain_specified=True, domain_initial_dot=True, path='/', path_specified=True, secure=False, expires=None, discard=True, comment=None, comment_url=None, rest={}, rfc2109=False)
        c = Cookie(version=0, name='WMF-Last-Access', value='29-Nov-2015', port=None, port_specified=False, domain='commons.wikimedia.org', domain_specified=False, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=1451520000, discard=False, comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False)
        c = Cookie(None, "commonswikiSession", "dcbfd70745be45768b2918210e5aa4f1", None, False, "commons.wikimedia.org",True, False, "/", None, True, False, False, None, None, None)
        c = Cookie(None, "commonswikiUserID", 5298875, None, False, "commons.wikimedia.org",True, False, "/", None, True, False, False, None, None, None)
        c = Cookie(None, "commonswikiUserName", "Acaronte", None, False, "commons.wikimedia.org",True, False, "/", None, True, False, False, None, None, None)
        c = Cookie(version=0, name='forceHTTPS', value='true', port=None, port_specified=False, domain='commons.wikimedia.org', domain_specified=False, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=1451355118, discard=False, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        filename = wiki.cookiepath + str(hash('acaronte' + ' - ' + wiki.apibase)) + '.cookies'

        '''
        old_umask = os.umask(0077)
        user = str(username).lower()
        filename = str(hash(user+' - '+wiki.apibase))+'.cookies'

        f = open(filename, 'w')
        f.write('')
        content = ''
        #wiki.cookies.set_cookie(c)
        #wiki.cookies.set_cookie(c)
        c = Cookie(version=0, name='centralauth_Session', value=str(password), port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=True, expires=None, discard=True, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        cook = pickle.dumps(c, 2)
        f.write(cook + '|~|')
        #c = Cookie(version=0, name='centralauth_Token', value='6b3100dac3d7b04e0dd79eb4b5670f61', port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=True, expires=int(time.time())+1000, discard=False, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        #cook = pickle.dumps(c, 2)
        #f.write(cook + '|~|')
        c = Cookie(version=0, name='centralauth_User', value=str(username), port=None, port_specified=False, domain='.commons.wikimedia.org', domain_specified=True, domain_initial_dot=False, path='/', path_specified=True, secure=True, expires=int(time.time())+1296000, discard=False, comment=None, comment_url=None, rest={'httponly': None}, rfc2109=False)
        cook = pickle.dumps(c, 2)
        f.write(cook + '|~|')
        content+=str(int(time.time()))+'|~|' # record the current time so we can test for expiration later
        content+='site.limit = %d;' % int(wiki.limit) # This eventially might have more stuff in it
        f.write(content)
        f.close()
        os.umask(old_umask)

        log = wiki.login(username=user, password=True, remember=False, force=False )
        print log
        f = open(filename, 'r')
        cookies = f.read().split('|~|')
        saved = cookies[len(cookies)-2]
        if int(time.time()) - int(saved) > 1296000: # 15 days, not sure when the cookies actually expire...
            f.close()
            os.remove(filename)
        sitedata = cookies[len(cookies)-1]
        del cookies[len(cookies)-2]
        del cookies[len(cookies)-1]
        for c in cookies:
            cook = pickle.loads(c)
            print cook.is_expired
            print "carga"
            if cook.discard:
                print "discard"
                continue
            if cook.is_expired:
                print "expired"
                continue
            print cook
        print sitedata
        f.close()
        #wiki = None
        #wiki = wikitools.wiki.Wiki(wiki_url)
        print "cookies"
        print wiki.cookies
        print wiki
    except:
        print("Login NO Exitoso")
        x = None
        print sys.exc_info()
        #print ("Invalid Username or Password")
    return wiki

def upload_file(url, file, wiki):
    #caption = "This image " + file_name + " was uploaded by WarriorSoft "
    #print(wiki)
    #t = getTitle(file)
    #t = t.replace("\xe2\x80\x99s", "-")

    #print ("file name " + file_name)
    #print (picture)

    #print ("prueba " + str(fileobj))
    picture = wikitools.wikifile.File(wiki=wiki, title=getTitle(file, url, getObservatory(url)) + ".jpg")
    print picture
    fileobj = open(file, "r")
    print fileobj
        #u= picture.upload(fileobj, comment="", ignorewarnings=False, watch=True)
    print picture.upload(fileobj, comment="", ignorewarnings=False, watch=True)

    #print (("Uploaded the File " + file_name))
    #edit_file(file_name)

def edit_file(description,date, url, credit, lic, categor, file, file_name, wiki):
        #metadatos = getMetadata(file_name+".jpg")
    #print url
    #print url
    page_name = getTitle(file, url, getObservatory(url)).replace(" ", "_")
    #print page_name
    page_url = "File:" + page_name + ".jpg"
    #print page_url
    page = wikitools.Page(wiki, page_url, followRedir=True)
    #print page
    descripcion = """=={{int:filedesc}}==
{{Information|description={{en|1= """ + description + """}}
|date=""" + date + """
|source=""" + url + """
|author=""" + credit + """}}"""
    descripcion
   #print descripcion
    licencia = """
=={{int:license-header}}==
{{Custom license marker|year=2015|month=09|day=21}}
{{ESO}}"""
    # [[Ccategory: cacacacca]]
    categorias = ""
    spl = categor.split("|")
    for c in spl:
        categorias += "[[Category: " +c+ " ]]\n"
    #print categorias
    #try:
    page.edit(text=(descripcion + licencia + categorias).encode('utf-8'))
        #print "wwww"+edd
        #print("Edicion Completada")
     #   return True
   # except:
        #print("Error, Editando... Puede que la imagen ya se encuentre en commons")
      #  return False
        #getMetadata(file_name+".jpg")
def getObservatory(url):
    if 'eso' in url:
        return "eso"
    elif 'spacetelescope' in url:
        return "spacetelescope"
    elif 'herschel' in url:
        return "herschel"

def getName(URls):
    try:
        spl = getUrlImage(URls, getObservatory(URls)).split("/")
        nombre = spl[len(spl) - 1]
        #print (nombre)
        return filename(nombre) + ".jpg"
    except:
        return("Sin Nombre")

def getUrlImage(URls,observatory):
    try:
        url = URls
        jquery = pq(url=url)
        elementos = [elemento.attr('src') for elemento in jquery.items('img')]
        #print (elementos[0])
        if(observatory == "eso"):
            URL = elementos[0]
            spl = URL.split("/")
            nombre = spl[len(spl) - 1]
            cat = spl[len(spl) - 2]
            #print cat
            if(cat != "screen"):
                URL = "http://cdn.eso.org/images/screen/"+nombre
        elif(observatory == "spacetelescope"):
            URL = elementos[0]
            spl = URL.split("/")
            nombre = spl[len(spl) - 1]
            cat = spl[len(spl) - 2]
            #print cat
            if(cat != "screen"):
                URL = "http://cdn.eso.org/images/screen/"+nombre
        elif(observatory == "herschel"):
            URL = "http://www.herschel.caltech.edu"+elementos[3]

        return URL
    except:
        return("Not URL Image")

def downloadImage(URls, observatory):
    try:
        #print observatory
        url = URls
        jquery = pq(url=url)
        elementos = [elemento.attr('src') for elemento in jquery.items('img')]
        #print (elementos[0])
        if(observatory == "eso"):
            #print "esooo"
            URL = elementos[0]
            spl = URL.split("/")
            nombre = spl[len(spl) - 1]
            cat = spl[len(spl) - 2]
            #print cat
            if(cat != "screen"):
                URL = "http://cdn.eso.org/images/screen/"+nombre
        elif(observatory == "spacetelescope"):
            #print "space"
            URL = elementos[0]
            spl = URL.split("/")
            nombre = spl[len(spl) - 1]
            cat = spl[len(spl) - 2]
            #print cat
            if(cat != "screen"):
                URL = "http://cdn.eso.org/images/screen/"+nombre
        elif(observatory == "herschel"):
            #print "herschel"
            URL = "http://www.herschel.caltech.edu"+elementos[3]
            spl = URL.split("?")
            URL = spl[0]
            spl = URL.split("/")
            nombre = spl[len(spl) - 1]

        #print URL + "que obtengo"
        #print (nombre)
        urllib.urlretrieve(URL, nombre)
        #print (" [+] Se ha Descargado la Imagen")
    except:
        return("No Descarga imagen")

def getTitle(file,urls,observatory):

    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        #print "antes"
        title = format(metadata['Xmp.dc.title'].raw_value)
        title = title[15:len(title) - 2]
        title = title.replace('\\xe2\\x80\\x99', "'")
        title = title.replace('\\xc2\\xa0', " ")
        title = title.replace('\\xe2\\x80\\x94', "-")
        title = title.replace('\\xc3\\xbc', "ü")
        title = title.replace('\\xc3\\xa9', "ä")
        title = title.replace('\\xe2\\x80\\x9c', '"')
        title = title.replace('\\xe2\\x80\\x9d', '"')
        title = title.replace('\\xc3\\xad', 'í')
        title = title.replace('\\xc3\\xa1', 'á')
        title = title.replace('\\xc3\\xa9', 'é')
        title = title.replace('\\xc3\\xb3', 'ó')
        title = title.replace('\\xc3\\xba', 'ú')
        title = title.replace('\\xc3\\xab', 'ë')
        title = title.replace('\\xc3\\xaf', 'ï')
        title = title.replace('\\xc3\\xb6', 'ö')
        #print title
        return title.encode('utf-8')
    except:
        jQuery = pq(url=urls)
        if(observatory == "herschel"):
            tit = jQuery("h2").text()
        else:
            tit = jQuery("h1").text()

        #print "scraping " + tit
        return tit.encode('utf-8')



def Meta(archivo):
    metadata = ImageMetadata(archivo)
    metadata.read()
    if metadata.xmp_keys:
        for key in metadata.xmp_keys:
            print("{}: {}".format(key, metadata[key].raw_value))

def getDescription(file, urls):
    #print urls
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        #metadatas = []
        #print "antes"
        descr = format(metadata['Xmp.dc.description'].raw_value)
        #print descr.decode('utf8')
        descr = descr[15:len(descr) - 2]
        #descr = descr.encode('windows-1252')
        descr = str(descr)
        descr = descr.replace('\\xe2\\x80\\x99', "'")
        descr = descr.replace('\\xc2\\xa0', " ")
        descr = descr.replace('\\xe2\\x80\\x94', "-")
        descr = descr.replace('\\xc3\\xbc', "ü")
        descr = descr.replace('\\xc3\\xa9', "ä")
        descr = descr.replace('\\xe2\\x80\\x9c', '"')
        descr = descr.replace('\\xe2\\x80\\x9d', '"')
        descr = descr.replace('\\xc3\\xad', 'í')
        descr = descr.replace('\\xc3\\xa1', 'á')
        descr = descr.replace('\\xc3\\xa9', 'é')
        descr = descr.replace('\\xc3\\xb3', 'ó')
        descr = descr.replace('\\xc3\\xba', 'ú')
        descr = descr.replace('\\xc3\\xab', 'ë')
        descr = descr.replace('\\xc3\\xaf', 'ï')
        descr = descr.replace('\\xc3\\xb6', 'ö')
        #descr = descr.decode(encoding)
        #metadatas.append(descr)
        #print u', '.join(['%r'%e for e in descr])
        return descr
    except:
        #print "no hay metadatos"
        jQuery = pq(url=urls)
        descri = ""
        #descri = jQuery("p.p1").text()
        for x in jQuery("p.p1"):
            descri += jQuery(x).text()+"\n"
        #print descri
        return descri.encode('utf-8')

def getCredits(file,urls):
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        c = format(metadata['Xmp.photoshop.Credit'].raw_value)
        return c.encode('utf-8')
    except:
        jQuery = pq(url=urls)
        return (jQuery("div.credit").eq(0).text().encode('utf-8') )

def getDate(file,urls):
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        fdc = format(metadata['Xmp.photoshop.DateCreated'].raw_value)
        return fdc
    except:
        jQuery = pq(url=urls)

        return jQuery("td").eq(5).text()

def getSource(file):
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        udr = format(metadata['Xmp.avm.ReferenceURL'].raw_value)
        return udr
    except:
        return "No es posible Obtener URL referencia"

def getAutor(file):
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        ndp = format(metadata['Xmp.avm.Publisher'].raw_value)
        return ndp
    except:
        return "No es posible Obtener Autor"

def getSubject(file):
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        pal_claves = format(metadata['Xmp.dc.subject'].raw_value)
        return getList(pal_claves)
    except:
        return "None subject"

def getLicence(file):
    try:
        image = file
        metadata = ImageMetadata(image)
        metadata.read()
        lic = format(metadata['Xmp.xmpRights.UsageTerms'].raw_value)
        lic = lic[15:len(lic) - 2]
        return (lic)
    except:
        return " Creative Commons Attribution 4.0 International License"


def getList(string):
    try:
        test1 = string.split(",")
        test2 = []
        x = 0
        for i in test1:
            if(x == 0):
                test2.append(i[2:-1])
            elif(x == (len(test1)) - 1):
                test2.append(i[2:-2])
            else:
                test2.append(i[1:-1])
            x = x + 1
        return test2
    except:
        return("No Lista")

# Keywords


def keywords(test_url):
    try:
        response = alchemyapi.keywords('url', test_url)
        key = response['keywords']
        keyword = []
        for i in range(len(key)):
            if float(key[i]['relevance']) >= 0.6:
                keyword.append(key[i]['text'])
        return keyword
    except:
        return ""

# Category


def category(test_url):
    try:
        response = alchemyapi.category('url', test_url)
        category = []
        category = response['category']
        return category
    except:
        return("no category")


def filename(file):
    return (file.split(".")[-2])


def Categorias(file, page_url):
    try:
        categorias_1 = ""
        for aux in getArrayCategory(file, page_url):
            categorias_1 += "[[Category:" + aux + "]]\n"
        #print(categorias_1)
        return categorias_1
    except:
        return("No Categorias")


def getCategory(file, page_url):
    try:
        categorias_1 = ""
        for aux in getArrayCategory(file, page_url):
            categorias_1 += (aux + """|""")
        categorias_1 = categorias_1[:len(categorias_1) - 1]
        #print(categorias_1)
        return categorias_1.encode('utf-8')
    except:
        return "no categorys"

def getArrayCategory(file, page_url):
    try:
        categorias = []
        #categorias.append("Picture Upload by WarriorSoft-Team")
        palabras_claves = getSubject(file)
        for cat in palabras_claves:
            x = cat[len(cat) - 1:len(cat)]
            if(x == "'"):
                cat = cat[:len(cat) - 1]
            y = cat[0:1]
            if(y == "'"):
                cat = cat[1:len(cat)]
            categorias.append(cat)
        for cat in keywords(page_url):
            if((cat == "ESO Sites                           Weekend")):
                categorias.append("ESO Sites Weekend")
            else:
                categorias.append(cat)
        cat = category(page_url)
        categorias.append(cat)
        return categorias
    except:
        print("Sin getArrayCategory")

def URLWikipedia(file):

    articles = []
    palabras_claves = getSubject(file)
    for url in palabras_claves:
        try:
            page_name = url.replace(" ", "_")
            x = page_name[len(page_name) - 1:len(page_name)]
            if(x == "'"):
                page_name = page_name[:len(page_name) - 1]
            y = page_name[0:1]
            if(y == "'"):
                page_name = page_name[1:len(page_name)]
            URL = "https://en.wikipedia.org/wiki/"
            #print(page_name)
            url_final = URL + page_name
            #print(url_final)
            response = requests.get(url_final)
            rta = response.status_code
            if(rta == 200):
                #print(("[+] OK ->    " + url_final))
                articles.append(url_final)
        except:
            x = None
            print(("NOT URLWikipedia"))
    return articles

def getArticles(file):
    try:
        srt=""
        for url in URLWikipedia(file):
            srt += (url + "|")
        srt = srt[:len(srt) - 1]
        #print srt
        return srt
    except:
        return "No es posible Obtener Articulos|"

def codigoImageWikipedia(title, url_article, date):
    try:
        t = title.replace(" ", "_")
        codigo = """[[File:"""+t+""".jpg|thumb|300px|{{cite web |url="""+url_article+""" |title="""+title+""" |date="""+date+"""}}]]"""
        #print codigo
        return codigo.encode('utf-8')
    except:
        return "No es posible Obtener Codigo"

def getURLWikimedia(Title_image):
    t = Title_image.replace(" ", "_")
    url = """https://commons.wikimedia.org/wiki/File:"""+t+""".jpg"""
    #print url
    return url.encode('utf-8')


# This Script was c0ded by WarriosSoft-Team


