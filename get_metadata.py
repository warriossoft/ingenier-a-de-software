#!/usr/bin/env python

#	Copyright 2013 AlchemyAPI
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from __future__ import print_function
from alchemyapi import AlchemyAPI

test_url = 'http://www.eso.org/public/images/potw1534a/'
#test_url = 'http://www.eso.org/public/images/potw1538a/'
#test_url = 'http://www.eso.org/public/images/potw1537a/'
#test_url = 'http://www.eso.org/public/images/potw1536a/'
#test_url = 'http://www.eso.org/public/images/potw1535a/'
#test_url = 'http://www.eso.org/public/images/potw1430a/'
#test_url = 'http://www.eso.org/public/images/potw1449a/'
#test_url = 'http://www.eso.org/public/images/potw1452a/'
#test_url = 'http://www.eso.org/public/images/potw1451a/'
#test_url = 'http://www.eso.org/public/images/potw1450a/'
alchemyapi = AlchemyAPI()

# Keywords
def keywords():
    response = alchemyapi.keywords('url', test_url)
    key = response['keywords']
    keyword = []
    for i in range(len(key)):
        if float(key[i]['relevance']) >= 0.70:
            keyword.append(key[i]['text'])
    return keyword

# Category
def category():
    response = alchemyapi.category('url', test_url)
    category = []
    category = response['category']
    return category