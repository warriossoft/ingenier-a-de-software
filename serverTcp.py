from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory
import thread
import esoWiki_WS
import sys

threads = list()
hilo_terminado = False
hilo2_terminado = False
Url_image = ""
Title_image = ""
Description_image=""
Credits_image = ""
Licence_image = ""
Url_origin = None
Date_image = ""
Keywords_image = ""
File = None
login=None
class MyServerProtocol(WebSocketServerProtocol):

    def onConnect(self, request):
        x = None
        print("Client connecting: {0}".format(request.peer))

    def onOpen(self):
        x = None
        self.sendMessage("conected")

    def onMessage(self, payload, isBinary):
        if isBinary:
            x = None
            #print("Binary message received: {0} bytes".format(len(payload)))
        else:
            global Url_image
            global Title_image
            global Description_image
            global Credits_image
            global Licence_image
            global Url_origin
            global Date_image
            global Keywords_image
            global File
            global login
            mensaje = format(payload.decode('utf8'))
            if(mensaje[:11] == "Credentials"):
                user=mensaje.split("***")[1]
                password=mensaje.split("***")[2]
                login = esoWiki_WS.Login(user,password)
                print login
                self.sendMessage("llegaron las credentials")

            if(mensaje[:32] == "http://www.herschel.caltech.edu/"):
                url = mensaje
                thread.start_new_thread(met, (url,File,"herschel",self,isBinary))


            if(mensaje[:30] == "http://www.spacetelescope.org/"):
                url = mensaje
                File = esoWiki_WS.getName(url)

                spl = url.split("/")
                #print file_name
                catego = spl[len(spl) - 3]
                #print catego
                if(catego != "images"):
                    url = "http://www.spacetelescope.org/images/"+esoWiki_WS.getName(url)[:-4]+"/"

                #print url
                #print "file " + File
                thread.start_new_thread(met, (url,File,"spacetelescope",self,isBinary))


            if(mensaje[:26] == "http://www.eso.org/public/"):
                url = mensaje
                File = esoWiki_WS.getName(url)

                spl = url.split("/")
                #print file_name
                catego = spl[len(spl) - 3]
                #print catego
                if(catego != "images"):
                    url = "http://www.eso.org/public/images/"+esoWiki_WS.getName(url)[:-4]+"/"

               # print url
                #print "file " + File
                thread.start_new_thread(met, (url,File,"eso",self,isBinary))

            if(mensaje[:6] == "Url***"):
                Url_image = mensaje.split("***")[1]
                #print Url_image

            if(mensaje[:8] == "Title***"):
                Title_image = mensaje.split("***")[1]
                #print Title_image

            if(mensaje[:14] == "Description***"):
                Description_image = mensaje.split("***")[1]
                #print Description_image

            if(mensaje[:10] == "Credits***"):
                Credits_image = mensaje.split("***")[1]
                #print Credits_image

            if(mensaje[:10] == "Licence***"):
                Licence_image = mensaje.split("***")[1]
                #print Licence_image

            if(mensaje[:13] == "Url_origin***"):
                Url_origin = mensaje.split("***")[1]
                #print Url_origin

            if(mensaje[:7] == "Date***"):
                Date_image = mensaje.split("***")[1]
                #print Date_image

            if(mensaje[:11] == "Keywords***"):
                #login = esoWiki_WS.Login("acaronte","f63e1a3fc539df4ea7d93c44decc2e43")
                Keywords_image = mensaje.split("***")[1]
                print Keywords_image
                #login = esoWiki_WS.Login()
                #print "url origine " + Url_origin
                File = esoWiki_WS.getName(Url_origin)
                print login
                #print File
                thread.start_new_thread(wiki, (Title_image, Description_image, Date_image, Url_origin,Credits_image,Licence_image,Keywords_image, File,login,self,isBinary))

    def onClose(self, wasClean, code, reason):
        x = None
        #print("WebSocket connection closed: {0}".format(reason))

def wiki(Title_image, Description_image, Date_image, Url_origin,Credits_image,Licence_image,Keywords_image, File, wiki, self, isBinary):
    global hilo2_terminado
    esoWiki_WS.upload_file(Url_origin, File, wiki)
    esoWiki_WS.edit_file(Description_image, Date_image, Url_origin,Credits_image,Licence_image,Keywords_image,  File, esoWiki_WS.filename(File), wiki)
    #self.sendMessage("Pagina Editada Satisfactoriamente", isBinary)
    #self.sendMessage("Ops***Al parecer esta imagen ya ha sido alojada por otro usuario y no se puede editar su descripcion " + File, isBinary)
    self.sendMessage("Code***"+esoWiki_WS.codigoImageWikipedia(Title_image,Url_origin,Date_image), isBinary)
    self.sendMessage("Articles***"+esoWiki_WS.getArticles(File), isBinary)
    url = "Url_wiki***" + esoWiki_WS.getURLWikimedia(Title_image)
    print (url)
    self.sendMessage(url, isBinary)

    hilo2_terminado = True

def met(url,File,observatory,self, isBinary):
    global hilo_terminado

    esoWiki_WS.downloadImage(url, observatory)
    try:
        #print "jeje"
        #esoWiki_WS.Meta(File) // imagen mini - titulo -descriptcion -creditos - licencia -url fuente - fecha de liberacion - categorias
        self.sendMessage("Url***" + esoWiki_WS.getUrlImage(url,observatory), isBinary)
        #print "Url***" + esoWiki_WS.getUrlImage(url,observatory)
        self.sendMessage("Title***" + esoWiki_WS.getTitle(File,url,observatory), isBinary)
        #print "Title***" + esoWiki_WS.getTitle(File,url,observatory)
        self.sendMessage("Description***" + esoWiki_WS.getDescription(File,url), isBinary)
        #print "Description***" + esoWiki_WS.getDescription(File,url)
        self.sendMessage("Credits***" + esoWiki_WS.getCredits(File,url), isBinary)
        #print "Credits***" + esoWiki_WS.getCredits(File,url)

        self.sendMessage("Licence***" + esoWiki_WS.getLicence(File), isBinary)
        #print "Licence***" + esoWiki_WS.getLicence(File)

        self.sendMessage("Url_origin***" + str(url), isBinary)
        #print "Url_origin***" + url

        self.sendMessage("Date***" + esoWiki_WS.getDate(File,url), isBinary)
        #print "Date***" + esoWiki_WS.getDate(File,url)

        self.sendMessage("Keywords***" + esoWiki_WS.getCategory(File, url), isBinary)
        #print "Keywords***" + esoWiki_WS.getCategory(File, url)
        #self.sendMessage("Article***" + esoWiki_WS.getArticles(File), isBinary)
        #print "Article***" + esoWiki_WS.getArticles(File)
        #self.sendMessage("Code***" + esoWiki_WS.codigoImageWikipedia(File), isBinary)
        #print "Code***" + esoWiki_WS.codigoImageWikipedia(File)

    except:
        self.sendMessage("Ha Ocurrido un error !!! Lo sentimos")
        print "Unexpected error:", sys.exc_info()[0]
        raise

    #else:
    #self.sendMessage("Ops***Al parecer esta imagen ya ha sido alojada por otro usuario y no se puede editar " + File, isBinary)
    hilo_terminado = True


if __name__ == '__main__':

    try:
        import asyncio
    except ImportError:
        # Trollius >= 0.3 was renamed
        import trollius as asyncio

    factory = WebSocketServerFactory("ws://0.0.0.0:1195", debug=False)
    factory.protocol = MyServerProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, '0.0.0.0', 1195)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
