import wikitools
import pyexiv2
import os
import shutil
#import poster
#wiki_url = "MediaWiki API url here"
#wiki_url = 'http://localhost/mediawiki/api.php'
wiki_url = 'http://commons.wikimedia.org/w/api.php'
wiki_username = "8a jhon mario"
wiki_password = "123456789jhon"
category = "Observation"
try:
    wiki = wikitools.wiki.Wiki(wiki_url)
except:
    print ("Can not connect with wiki. Check the URL")
try:
    wiki.login(username=wiki_username, password=wiki_password)
except:
    print ("Invalid Username or Password")

path = './'
listing = os.listdir(path)


def filetype(file):
    return file.split(".")[-1]


def filename(file):
    return file.split(".")[-2]


def move_file(file):
    source = file
    destination = "./uploaded/" + file
    if os.path.isdir("uploaded"):
        shutil.move(source, destination)
    else:
        os.mkdir("uploaded")
        shutil.move(source, destination)
    print (("Moving the file " + file + " to the folder 'uploaded' "))


def upload_file(file):
    file_name = filename(file)
    caption = "This image " + file_name + " was uploaded by WarriorSoft "
    picture = wikitools.wikifile.File(wiki=wiki, title=file_name + ".jpg")
    #print ("file name " + file_name)
    #print (picture)
    fileobj = open(file, "r")
    #print ("prueba " + str(fileobj))
    picture.upload(fileobj, comment=caption, ignorewarnings=False, watch=True)
    print (("Uploaded the File " + file_name))
    edit_file(file_name)


def edit_file(file_name):
    page_name = file_name.replace(" ", "_")
    page_url = "File:" + page_name + ".jpg"
    page = wikitools.Page(wiki, page_url, followRedir=True)
    descripcion = """=={{int:filedesc}}==
{{Information|description={{en|1= description}}
|date=2015-09-21 10:00:00
|source=http://www.eso.org/public/images/
|author=ESO}}"""
    licencia = """
=={{int:license-header}}==
{{Custom license marker|year=2015|month=09|day=21}}
{{ESO}}"""
    categorias = """[[Category:Uploaded with UploadWizard]]
[[Category:Very Large Telescope]]
[[Category:VLT]]
[[Category:Cerro Paranal]]
[[Category:Technology]]"""

    page.edit(text=(descripcion + licencia + categorias))
    #move_file(file)

#for photo in listing:
#    print photo
 #   print filetype(photo)
  #  if filetype(photo) in ['JPG', 'jpg', 'GIF', 'gif', 'png', 'PNG', 'ogg', 'OGG']:
upload_file('ann13016a.jpg')
